package lab1;

import java.util.Scanner; 

public class MainClass {

	public static void main(String[] args) {
		
		BigFraction  BG = new BigFraction();
		Scanner scan = new Scanner (System.in);
		
		System.out.println("��������� ������ ����� = ");
		String numeratorA = scan.nextLine();
		System.out.println("����������� ������ ����� = ");
		String denominatorA = scan.nextLine();
		System.out.println("��������� ������ ����� = ");
		String numeratorB = scan.nextLine();
		System.out.println("����������� ������ ����� = ");
		String denominatorB = scan.nextLine();
		

		
		// �����
		
		BigFraction sum = BG.sum(numeratorA, denominatorA, numeratorB, denominatorB);
		
		if (sum.isWarning) {
			
			System.out.println("����� - " + sum.warning + "\n" );
			
		}
		else {
			
			System.out.println("����� - " + sum.numerator + "/" + sum.denominator + "\n" );			
			
		}
		
		// ��������
		
		BigFraction dif = BG.dif(numeratorA, denominatorA, numeratorB, denominatorB);
		
		if (dif.isWarning) {
			
			System.out.println("�������� - " + dif.warning + "\n" );
			
		}
		else {
			
			System.out.println("�������� - " + dif.numerator + "/" + dif.denominator + "\n" );			
			
		}
		
		// ������������
		
		BigFraction mult = BG.mult(numeratorA, denominatorA, numeratorB, denominatorB);
		
		if (mult.isWarning) {
			
			System.out.println("������������ - " + mult.warning + "\n" );
			
		}
		else {
			
			System.out.println("������������ - " + mult.numerator + "/" + mult.denominator + "\n" );			
			
		}
		
		
		// �������
		
		BigFraction div = BG.div(numeratorA, denominatorA, numeratorB, denominatorB);
		
		if (div.isWarning) {
			
			System.out.println("������� - " + div.warning + "\n" );
			
		}
		else {
			
			System.out.println("������� - " + div.numerator + "/" + div.denominator + "\n" );			
			
		}		

	}

}
 