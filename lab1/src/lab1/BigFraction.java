package lab1;
import java.math.BigInteger;

/**
 * Class for addition, subtraction, multiplication and division of fractions with long arithmetic.
 */
public class BigFraction {
	
	BigInteger numerator, denominator;	
	String warning;	
	boolean isWarning;
	
	/**
	 * Constructor of the BigInteger class.
	 * Sets the warning messages.
	 * 
	 * @param String warning - a warning message.
	 */		
	public BigFraction() {
		
		this.warning = "none"; 
		
    }
	
	/**
	 * Constructor of the BigInteger class.
	 * Sets the error indicator and warning messages.
	 * 
	 * @param boolean isWarning - indicates if there is a validation error.
	 * @param String warning - a warning message.
	 */		
	public BigFraction(boolean isWarning, String warning) {
		
		this.isWarning = isWarning;		
		this.warning = warning;
		
    }	
	
	/**
	 * Constructor of the BigInteger class.
	 * Sets the resulting numerator and denominator.
	 * 
	 * @param BigInteger numerator - the resulting numerator.
	 * @param BigInteger denominator - the resulting denominator.
	 */	
	BigFraction(BigInteger numerator, BigInteger denominator) {
		
		this.numerator = numerator;
		this.denominator = denominator;	

    }
	
	/**
	 * BigFraction check
	 *
	 * @param String numeratorA - the numerator of the 1st fraction.
	 * @param String denominatorA - the denominator of the 1st fraction.
	 * @param String numeratorB - the numerator of the 2nd fraction.
	 * @param String denominatorB - the denominator of the 2nd fraction.
	 * 
	 * @return BigFraction object  where 
	 * 		   if validation is performed the property boolean isWarning = false 
	 * 		   else properties boolean isWarning = true and String warning = Warning message
	 */
	private BigFraction check(String numeratorA, String denominatorA, String numeratorB, String denominatorB) {
		
		if (!numeratorA.matches("-?\\d+") || 
			!numeratorB.matches("-?\\d+") || 
			!denominatorA.matches("-?\\d+") ||
			!denominatorB.matches("-?\\d+")) {
			
				BigFraction warning = new BigFraction(true, "Error - enter the number.");
				
				return warning;		
				
		}		
		else if (denominatorA == "0" || denominatorB == "0") {
				
				BigFraction warning = new BigFraction(true, "Error - enter the number.");
				
				return warning;		
				
			}		
		else {
				
				BigFraction warning = new BigFraction(false, "");
				
				return warning;		
				
			}
		
	}
	
	/**
	 * Adding fractions
	 *
	 * @param String numeratorA - the numerator of the 1st fraction.
	 * @param String denominatorA - the denominator of the 1st fraction.
	 * @param String numeratorB - the numerator of the 2nd fraction.
	 * @param String denominatorB - the denominator of the 2nd fraction.
	 *
	 * @return BigFraction object  with properties BigInteger numerator, BigInteger denominator or String warning
	 */
	public BigFraction sum(String numeratorA, String denominatorA, String numeratorB, String denominatorB) {
		
		if (this.check(numeratorA, denominatorA, numeratorB, denominatorB).isWarning) {
			
			return this.check(numeratorA, denominatorA, numeratorB, denominatorB);
			
		}
		
		BigInteger numerator, denominator, gcd;
		
		BigInteger nA = new BigInteger(numeratorA);
		BigInteger nB = new BigInteger(numeratorB);
		BigInteger dA = new BigInteger(denominatorA);
		BigInteger dB = new BigInteger(denominatorB);
		
	    if(dA != dB) {
	    	
		  numerator =  (nA.multiply(dB)).add(nB.multiply(dA));
		  denominator = dA.multiply(dB);
		  
	    }	    
	    else {
	    	
		  numerator =  nA.add(nB);
		  denominator = dA;	  
		  
	    }
	    
	    gcd = numerator.gcd(denominator);
	    numerator =  numerator.divide(gcd);
	    denominator =  denominator.divide(gcd);
	    
	    BigFraction result = new BigFraction(numerator, denominator);
	    
	    return result;
	    
	}
	
	/**
	 * Subtract fractions
	 *
	 * @param String numeratorA - the numerator of the 1st fraction.
	 * @param String denominatorA - the denominator of the 1st fraction.
	 * @param String numeratorB - the numerator of the 2nd fraction.
	 * @param String denominatorB - the denominator of the 2nd fraction.
	 *
	 * @return BigFraction object  with properties BigInteger numerator, BigInteger denominator or String warning
	 */
	public BigFraction dif(String numeratorA, String denominatorA, String numeratorB, String denominatorB) {
		
		if (this.check(numeratorA, denominatorA, numeratorB, denominatorB).isWarning) {
			
			return this.check(numeratorA, denominatorA, numeratorB, denominatorB);
			
		}
		
		BigInteger numerator, denominator, gcd;
		
		BigInteger nA = new BigInteger(numeratorA);
		BigInteger nB = new BigInteger(numeratorB);
		BigInteger dA = new BigInteger(denominatorA);
		BigInteger dB = new BigInteger(denominatorB);
		
	    if(dA != dB) {
	    	
		  numerator =  (nA.multiply(dB)).subtract(nB.multiply(dA));
		  denominator = dA.multiply(dB);
		  
	    }	    
	    else {
	    	
		  numerator =  nA.subtract(nB);
		  denominator = dA;	    	
		  
	    }
	    
	    gcd = numerator.gcd(denominator);
	    numerator =  numerator.divide(gcd);
	    denominator =  denominator.divide(gcd);

	    BigFraction result = new BigFraction(numerator, denominator);
	    
	    return result;
	    
	}
	
	/**
	 * Multiplying fractions
	 *
	 * @param String numeratorA - the numerator of the 1st fraction.
	 * @param String denominatorA - the denominator of the 1st fraction.
	 * @param String numeratorB - the numerator of the 2nd fraction.
	 * @param String denominatorB - the denominator of the 2nd fraction.
	 *
	 * @return BigFraction object  with properties BigInteger numerator, BigInteger denominator or String warning
	 */
	public BigFraction mult(String numeratorA, String denominatorA, String numeratorB, String denominatorB) {
		
		if (this.check(numeratorA, denominatorA, numeratorB, denominatorB).isWarning) {
			
			return this.check(numeratorA, denominatorA, numeratorB, denominatorB);
			
		}
		
		BigInteger numerator, denominator, gcd;
		
		BigInteger nA = new BigInteger(numeratorA);
		BigInteger nB = new BigInteger(numeratorB);
		BigInteger dA = new BigInteger(denominatorA);
		BigInteger dB = new BigInteger(denominatorB);
		
		numerator =  nA.multiply(nB);
		denominator = dA.multiply(dB);	    	
	    
	    gcd = numerator.gcd(denominator);
	    numerator =  numerator.divide(gcd);
	    denominator =  denominator.divide(gcd);
	    
	    BigFraction result = new BigFraction(numerator, denominator);
	    
	    return result;
	}
	
	/**
	 * Division fractions
	 *
	 * @param String numeratorA - the numerator of the 1st fraction.
	 * @param String denominatorA - the denominator of the 1st fraction.
	 * @param String numeratorB - the numerator of the 2nd fraction.
	 * @param String denominatorB - the denominator of the 2nd fraction.
	 *
	 * @return BigFraction object  with properties BigInteger numerator, BigInteger denominator or String warning
	 */
	public BigFraction div(String numeratorA, String denominatorA, String numeratorB, String denominatorB) {
		
		if (this.check(numeratorA, denominatorA, numeratorB, denominatorB).isWarning) {
			
			return this.check(numeratorA, denominatorA, numeratorB, denominatorB);
			
		}
		
		BigInteger numerator, denominator, gcd;
		
		BigInteger nA = new BigInteger(numeratorA);
		BigInteger nB = new BigInteger(numeratorB);
		BigInteger dA = new BigInteger(denominatorA);
		BigInteger dB = new BigInteger(denominatorB);
		
		numerator =  nA.multiply(dB);
		denominator = dA.multiply(nB);	    	
	    
	    gcd = numerator.gcd(denominator);
	    numerator =  numerator.divide(gcd);
	    denominator =  denominator.divide(gcd);
	    
	    BigFraction result = new BigFraction(numerator, denominator);
	    
	    return result;
	}
}
